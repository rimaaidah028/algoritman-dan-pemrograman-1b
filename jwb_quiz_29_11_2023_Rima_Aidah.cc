include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    int i = 0, N = 0;  // counter and number of data
    float Data = 0.0, Rata = 0.0, Total = 0.0, Banyaknya = 0.0;

    cout << "Input banyaknya data  : ";
    cin >> N;

    Total = 0;
    i = 1;

    do {
        cout << "Masukan Data ke-" << i << " : ";
        cin >> Data;
        Total += Data;

        if (Data != 0) {
            Banyaknya++;
        }
        i++;
    } while (i <= N);

    Rata = Total / Banyaknya;

    cout << "Banyaknya Data       : " << Banyaknya << endl;
    cout << "Total Nilai Data     : " << fixed << setprecision(2) << Total << endl;
    cout << "Rata-rata nilai Data : " << fixed << setprecision(2) << Rata << endl;

    return 0;
}
