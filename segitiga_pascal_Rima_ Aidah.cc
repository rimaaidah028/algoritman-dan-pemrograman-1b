#include <iostream>
using namespace std;

int main() {
    int N;
    cout << "Masukkan N: ";
    cin >> N;

    for (int i = 0; i < N; ++i) {
        int factorial = 1;

        for (int j = 0; j < N - i - 1; ++j) {
            cout << " ";
        }

        for (int j = 0; j <= i; ++j) {
            cout << factorial << " ";
            factorial = factorial * (i - j) / (j + 1);
        }
        cout << endl;
    }

    return 0;
}
