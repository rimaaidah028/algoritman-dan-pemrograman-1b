#include <iostream>
using namespace std;

int main(){
    int pil, n, r;
    cout << "1. Permutasi\n";
    cout << "2. Kombinasi\n";
    cout << "Pilih operasi yang ingin dilakukan:\n";
    cin >> pil;

    switch (pil){
        case 1:
          cout << "Masukkan Bilangan 1:";
          cin >> n;
          cout << "Masukkan Bilangan 2:";
          cin >> r;
          if(n >= r)
            cout << "Permutasi dari" << n << "dan" << r << "adalah:" << permutation(n, r) << endl;
            else
              cout << "Bilangan 1 harus lebih besar atau sama dengan Bilangan 2." << endl;
              break; 

              case 2:
                cout << "Masukkan Bilangan 1: ";
                cin >> n;
                cout << "Masukkan Bilangan 2: ";
                cin >> r;
                if (n >= r)
                cout << "Kombinasi dari " << n << " dan " << r << " adalah: " << combination(n, r) << endl;
                else
                cout << "Bilangan 1 harus lebih besar atau sama dengan Bilangan 2." << endl;
              break;

            default:
              cout << "Pilihan tidak valid." << endl;
              break;
    }

    return 0;
}
